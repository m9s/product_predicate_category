# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Not, Bool


STATES = {
    'readonly': Not(Bool(Eval('active'))),
}
DEPENDS = ['active']
SEPARATOR = u'/'


class PredicateCategory(ModelSQL, ModelView):
    "Predicate Category"
    _name = "product.template.predicate_category"
    _description = __doc__

    name = fields.Char('Name', required=True, states=STATES, translate=True,
        depends=DEPENDS)
    parent = fields.Many2One('product.template.predicate_category', 'Parent',
        select=1, states=STATES, depends=DEPENDS)
    children = fields.One2Many('product.template.predicate_category',
        'parent', 'Children', states=STATES, depends=DEPENDS)
    active = fields.Boolean('Active')

    def __init__(self):
        super(PredicateCategory, self).__init__()
        self._sql_constraints = [
            ('name_parent_uniq', 'UNIQUE(name, parent)',
                'The name of a product predicate category must be unique '
                'by parent!'),
        ]
        self._constraints += [
            ('check_recursion', 'recursive_predicate_categories'),
            ('check_name', 'wrong_name'),
        ]
        self._error_messages.update({
            'recursive_predicate_categories': 'You can not create '
                'recursive predicate categories!',
            'wrong_name': 'The use of "%s" character in name field '
                'is not allowed!' % SEPARATOR,
        })

    def default_active(self):
        return True

    def check_name(self, ids):
        for predicate_category in self.browse(ids):
            if SEPARATOR in predicate_category.name:
                return False
        return True

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        def _name(predicate_category):
            if predicate_category.id in res:
                return res[predicate_category.id]
            elif predicate_category.parent:
                return (_name(predicate_category.parent)
                    + ' '+ SEPARATOR + ' ' + predicate_category.name)
            else:
                return predicate_category.name
        for predicate_category in self.browse(ids):
            res[predicate_category.id] = _name(predicate_category)
        return res

    def search_rec_name(self, name, clause):
        if isinstance(clause[2], basestring):
            values = clause[2].split(' '+SEPARATOR+' ')
            values.reverse()
            domain = []
            field = 'name'
            for name in values:
                domain.append((field, clause[1], name))
                field = 'parent.' + field
            ids = self.search(domain, order=[])
            return [('id', 'in', ids)]
        #TODO Handle list
        return [('name',) + tuple(clause[1:])]

PredicateCategory()
