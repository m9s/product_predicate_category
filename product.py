# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Template(ModelSQL, ModelView):
    _name = "product.template"

    predicate_categories = fields.Many2Many(
        'product.template-product.predicate_category','template',
        'predicate_category', 'Predicate Categories')

Template()


class ProductTemplatePredicateCategory(ModelSQL):
    'Product - Predicate Category'
    _name = 'product.template-product.predicate_category'
    _table = 'product_predicate_rel'
    _description = __doc__

    template = fields.Many2One('product.template', 'Product',
        ondelete='CASCADE', required=True, select=1)
    predicate_category = fields.Many2One('product.template.predicate_category',
       'Predicate Category', ondelete='RESTRICT', required=True, select=1)

ProductTemplatePredicateCategory()
